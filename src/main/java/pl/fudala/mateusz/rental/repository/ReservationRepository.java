package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.model.car.CarEntity;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<ReservationEntity, Long> {

    @Query(value = "select r2.id, r2.price, r2.rental_end_date," +
            " r2.rental_start_date, r2.reservation_date," +
            " r2.department_arrival_id, car_id, customer_id, department_departure_id" +
            " from rental.rental.reservation r2 \n" +
            "left join rental.rental.reservation_revenue rr\n" +
            "on r2.id = reservation_id \n" +
            "where rr.realised = 'false'; ", nativeQuery = true)
    List<ReservationEntity> findAllNotRealised();

    @Transactional
    @Query(value = "SELECT r FROM reservation r WHERE ?1 BETWEEN r.rentalStartDate AND r.rentalEndDate")
    List<ReservationEntity> findAllReservedCarsInProvidedDate(LocalDateTime date);
}
