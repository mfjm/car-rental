package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.fudala.mateusz.rental.model.agreements.RentalEntity;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RentalRepository extends JpaRepository<RentalEntity, Long> {

    @Transactional
    @Query(value = "SELECT ren FROM rental ren JOIN reservation res ON ren.reservation.id = res.id " +
            "WHERE ?1 BETWEEN res.rentalStartDate AND res.rentalEndDate")
    List<RentalEntity> findAllRentedCarsInProvidedDate(LocalDateTime dateTime);

}
