package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.fudala.mateusz.rental.model.car.CarEntity;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<CarEntity, Long> {
    List<CarEntity> findByMakeIgnoreCase(String make);

    @Transactional
    @Query(value = "SELECT c FROM car c WHERE c.department.id = ?1 and c.rentalStatus = 'AVAILABLE'")
    List<CarEntity> findAvailableCarsByDepartmentId(Long departmentId);

    @Transactional
    @Query(value = "select c from car c " +
            "join reservation r on c.id = r.car.id " +
            "join reservation_revenue rr on r.id = rr.reservationId " +
            "where rr.realised = false and ?1 between r.rentalStartDate and r.rentalEndDate")
    List<CarEntity> findAllReservedCarsInProvidedDate(LocalDateTime dateTime);

    @Transactional
    @Query(value = "select c from car c " +
            "join reservation r on c.id = r.car.id " +
            "join rental r2 on r.id = r2.reservation.id " +
            "where ?1 between r.rentalStartDate and r.rentalEndDate and r2.returned = false")
    List<CarEntity> findAllRentedCarsInProvidedDate(LocalDateTime dateTime);

    @Transactional
    @Query(value = "select c from car c " +
            "join unavailable u on c.id = u.car.id " +
            "where ?1 between u.startDate and u.endDate")
    List<CarEntity> findAllUnavailableCarsInProvidedDate(LocalDateTime dateTime);

}