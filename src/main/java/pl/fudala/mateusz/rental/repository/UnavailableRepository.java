package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.fudala.mateusz.rental.model.agreements.UnavailableEntity;

@Repository
public interface UnavailableRepository extends JpaRepository<UnavailableEntity, Long> {
}
