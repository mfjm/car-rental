package pl.fudala.mateusz.rental.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.fudala.mateusz.rental.model.revenues.ReturnRevenueEntity;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface ReturnRevenueRepository extends JpaRepository<ReturnRevenueEntity, Long> {
    Page<ReturnRevenueEntity> findAll(Pageable pageable);

    @Query(value = "SELECT SUM(returnSurcharge) FROM return_revenue")
    Optional<BigDecimal> getSumOfRentalRevenues();
}
