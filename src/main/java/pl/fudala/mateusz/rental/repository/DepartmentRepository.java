package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {
    DepartmentEntity findByDepartmentName(String departmentName);

    @Transactional
    @Query(value = "SELECT d FROM department d WHERE d.id <> :currentEditedDepartment")
    List<DepartmentEntity> findDepartmentsWithCars(@Param("currentEditedDepartment") Long currentEditedDepartment);

    @Transactional
    @Query(value = "SELECT DISTINCT(d) FROM department d JOIN car c on d.id = c.department WHERE c.rentalStatus = 'AVAILABLE'")
    List<DepartmentEntity> findDepartmentsWithAvailableCars();



}
