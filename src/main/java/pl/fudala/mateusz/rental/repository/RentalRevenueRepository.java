package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.fudala.mateusz.rental.model.revenues.RentalRevenueEntity;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface RentalRevenueRepository extends JpaRepository<RentalRevenueEntity, Long> {

    @Query(value = "SELECT SUM(rentalValue) FROM rental_revenue")
    Optional<BigDecimal> getSumOfRentalRevenues();

    @Query(value = "SELECT SUM(SUM) FROM (\n" +
            "SELECT SUM(surcharge) AS SUM FROM rental.return \n" +
            "UNION ALL\n" +
            "SELECT SUM(SUM) FROM (SELECT SUM(rental_value) FROM rental.rental_revenue) SUMALL \n" +
            "UNION ALL \n" +
            "SELECT SUM(SUM)  FROM \n" +
            "(  \n" +
            "SELECT SUM(reservation_revenue.reservation_value) * 0.2 \n" +
            "AS SUM FROM rental.reservation_revenue WHERE cancelled = 'true' AND realised = 'false' \n" +
            "UNION ALL \n" +
            "SELECT SUM(reservation_revenue.reservation_value) \n" +
            "AS SUM FROM rental.reservation_revenue WHERE cancelled = 'false' AND realised = 'false' \n" +
            ") SUM) SUM ;", nativeQuery = true)
    Optional<BigDecimal> getSumOfAllRevenues();
}
