package pl.fudala.mateusz.rental.dto.person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class AddressDto {

    private Long id;
    private String city;
    private String postalCode;
    private String street;
    private String houseNumber;

    @Override
    public String toString() {
        return postalCode + " " + city + ", " + street + " " + houseNumber;
    }
}
