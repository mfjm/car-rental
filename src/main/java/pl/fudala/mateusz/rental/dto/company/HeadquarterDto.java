package pl.fudala.mateusz.rental.dto.company;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.company.AddressEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class HeadquarterDto {

    private String name;
    private String website;
    private String companyLogo;
    private AddressEntity address;
    private EmployeeEntity owner;
    private List<DepartmentEntity> departments;

}
