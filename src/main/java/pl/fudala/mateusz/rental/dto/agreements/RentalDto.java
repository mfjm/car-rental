package pl.fudala.mateusz.rental.dto.agreements;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class RentalDto {

    private Long id;
    private EmployeeEntity employee;
    private LocalDateTime rentalDate;
    private ReservationEntity reservation;
    private String additionalInfo;
    private boolean returned;


}
