package pl.fudala.mateusz.rental.model.agreements;

import lombok.*;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "return")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReturnEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private EmployeeEntity employee;
    private LocalDateTime returnDate;

    @OneToOne(cascade = CascadeType.ALL)
    private RentalEntity rental;

    @Getter
    private BigDecimal surcharge;
    private String additionalInfo;


}
