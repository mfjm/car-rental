package pl.fudala.mateusz.rental.model.person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.agreements.RentalEntity;
import pl.fudala.mateusz.rental.model.company.AddressEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.enums.JobTitle;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "employee")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;

    @ManyToOne
    @JoinColumn(name = "address_id",
            foreignKey = @ForeignKey(name = "address_id_fk"))
    private AddressEntity address;

    @ManyToOne
    private DepartmentEntity department;
    private JobTitle jobTitle;

    @OneToMany
    private List<RentalEntity> rentals;

}
