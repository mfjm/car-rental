package pl.fudala.mateusz.rental.model.enums;

import lombok.Getter;

public enum CarRentalStatus {
    RENTED("Car is rented."),
    AVAILABLE("Car is available."),
    UNAVAILABLE("Car is temporary unavailable."),
    RESERVED("Car is reserved for rent.");

    @Getter
    private String description;

    CarRentalStatus(final String description) {
        this.description = description;
    }
}
