package pl.fudala.mateusz.rental.model.company;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
@Entity(name = "department")
@Table(name = "department")
@Builder
@NoArgsConstructor
public class DepartmentEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String departmentName;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private AddressEntity address;

    @OneToMany
    private List<EmployeeEntity> employees = new ArrayList<>();

    @OneToMany(cascade = CascadeType.MERGE)
    private List<CarEntity> cars = new ArrayList<>();


    @Override
    public String toString() {
        return departmentName;
    }
}
