package pl.fudala.mateusz.rental.model.agreements;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity(name = "rental")
@Table(name = "rental")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RentalEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime rentalDate;

    @ManyToOne
    private EmployeeEntity employee;

    @OneToOne(cascade = CascadeType.ALL)
    private ReservationEntity reservation;
    private String additionalInfo;

    private boolean returned = false;

}
