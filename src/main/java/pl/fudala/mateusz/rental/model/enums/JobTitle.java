package pl.fudala.mateusz.rental.model.enums;

public enum JobTitle {
    MANAGER, ORDINARY_WORKER, OWNER
}
