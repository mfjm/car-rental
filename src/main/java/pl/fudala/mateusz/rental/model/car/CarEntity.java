package pl.fudala.mateusz.rental.model.car;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.enums.CarColor;
import pl.fudala.mateusz.rental.model.enums.CarRentalStatus;
import pl.fudala.mateusz.rental.model.enums.CarType;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity(name = "car")
@Table(name = "car")
@Builder
@AllArgsConstructor
@EqualsAndHashCode
public class CarEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String model;
    private String make;
    @Enumerated(EnumType.STRING)
    private CarType carType;
    private Long productionYear;
    @Enumerated(EnumType.STRING)
    private CarColor carColor;
    private Long mileage;
    @Enumerated(EnumType.STRING)
    private CarRentalStatus rentalStatus;
    private BigDecimal pricePerDay;

    @ManyToOne
    private DepartmentEntity department;

    @OneToOne(cascade = CascadeType.ALL)
    private ReservationEntity reservation;

    public CarEntity() {
        this.mileage = 0L;
    }

    public CarEntity(CarEntity carEntity) {
        this.setId(carEntity.getId());
        this.setDepartment(carEntity.getDepartment());
        this.setRentalStatus(carEntity.getRentalStatus());
        this.setCarColor(carEntity.getCarColor());
        this.setCarType(carEntity.getCarType());
        this.setMileage(carEntity.getMileage());
        this.setPricePerDay(carEntity.getPricePerDay());
        this.setRentalStatus(carEntity.getRentalStatus());
        this.setReservation(carEntity.getReservation());
        this.setModel(carEntity.getModel());
        this.setProductionYear(carEntity.getProductionYear());
        this.setMake(carEntity.getMake());
    }

    @Override
    public String toString() {
        return make + " " + model + " " + productionYear;
    }
}
