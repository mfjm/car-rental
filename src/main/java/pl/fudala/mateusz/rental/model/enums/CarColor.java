package pl.fudala.mateusz.rental.model.enums;

public enum CarColor {
    WHITE, RED, BLACK, BLUE, GREEN, YELLOW
}
