package pl.fudala.mateusz.rental.utils.pagination;

import org.springframework.data.domain.Page;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Paginator {

    private Paginator(){}

    private static <T> List<Integer> getListOfPageNumbers(Page<T> page) {
        return IntStream.rangeClosed(1, page.getTotalPages())
                .boxed()
                .collect(Collectors.toList());
    }

    public static <T> void addPageIndexes(ModelAndView mav, Page<T> listOfItems) {
        mav.addObject("totalNumberOfPages", listOfItems.getTotalPages());
        mav.addObject("numbersList", Paginator.getListOfPageNumbers(listOfItems));
    }
}
