package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import pl.fudala.mateusz.rental.dto.company.DepartmentDto;
import pl.fudala.mateusz.rental.service.AddressService;
import pl.fudala.mateusz.rental.service.CarService;
import pl.fudala.mateusz.rental.service.DepartmentService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;
import pl.fudala.mateusz.rental.utils.pagination.Paginator;

import java.util.stream.Collectors;

@RestController
@RequestMapping(DepartmentController.BASE_URL)
public class DepartmentController {

    public static final String BASE_URL = "/api/departments";

    private final DepartmentService departmentService;
    private final AddressService addressService;
    private final CarService carService;

    @Autowired
    public DepartmentController(DepartmentService departmentService, AddressService addressService, CarService carService) {
        this.departmentService = departmentService;
        this.addressService = addressService;
        this.carService = carService;
    }

    @GetMapping
    public ModelAndView allDepartments(@RequestParam(required = false) Integer page) {
        int pageNumber = (page == null ? 1 : page);
        int pageSize = 3;

        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, Sort.by("departmentName"));
        Page<DepartmentDto> departments = departmentService.findAll(pageable);

        ModelAndView mav = new ModelAndView("department/departments");
        mav.addObject("departments", departments);
        Paginator.addPageIndexes(mav, departments);

        return mav;
    }

    @GetMapping("add")
    public ModelAndView getDepartmentAddForm() {
        ModelAndView mav = new ModelAndView("department/add-department");
        mav.addObject("department", new DepartmentDto());
        mav.addObject("addresses", addressService.findAll());
        return mav;
    }

    @PostMapping("add")
    public ModelAndView addNewDepartment(@ModelAttribute("department") DepartmentDto department) {
        departmentService.addNewDepartment(department);
        return new ModelAndView("redirect:" + BASE_URL);
    }

    @GetMapping("/delete/{id}")
    public RedirectView deleteDepartment(@PathVariable Long id) {
        departmentService.deleteDepartmentById(id);
        return new RedirectView(BASE_URL);
    }

    @GetMapping("/edit/{id}")
    public ModelAndView getEditForm(@PathVariable Long id) {
        ModelAndView mav = new ModelAndView("department/edit-department");
        mav.addObject("department", departmentService.findById(id));
        mav.addObject("addresses", addressService.findAll());
        mav.addObject("cars", carService.findAll().stream().map(Mapper::map).collect(Collectors.toList()));
        return mav;
    }

    @PostMapping("/edit/{id}")
    public RedirectView editDepartment(@ModelAttribute("department") DepartmentDto departmentDto,
                                @PathVariable Long id) {
        departmentService.editDepartment(departmentDto, id);

        return new RedirectView(BASE_URL);
    }
}