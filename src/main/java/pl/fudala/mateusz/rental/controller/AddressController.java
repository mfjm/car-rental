package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import pl.fudala.mateusz.rental.dto.person.AddressDto;
import pl.fudala.mateusz.rental.service.AddressService;
import pl.fudala.mateusz.rental.utils.pagination.Paginator;

@RestController
@RequestMapping(AddressController.BASE_URL)
public class AddressController {

    public static final String BASE_URL = "/api/addresses";

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }


    @GetMapping
    public ModelAndView allAddresses(@RequestParam(required = false) Integer page) {
        int pageNumber = (page == null ? 1 : page);
        int pageSize = 2;

        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, Sort.by("city"));
        Page<AddressDto> addresses = addressService.findAll(pageable);

        ModelAndView mav = new ModelAndView("address/addresses");
        mav.addObject("addresses", addresses);
        Paginator.addPageIndexes(mav,addresses);

        return mav;
    }

    @GetMapping("add")
    public ModelAndView getAddressAddForm() {
        ModelAndView mav = new ModelAndView("address/add-address");
        mav.addObject("address", new AddressDto());
        return mav;
    }

    @PostMapping("add")
    public ModelAndView addNewAddress(@ModelAttribute("address") AddressDto address) {
        addressService.addNewAddress(address);
        return new ModelAndView("redirect:" + BASE_URL);
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteAddress(@PathVariable Long id) {
        addressService.deleteAddressById(id);
        return new ModelAndView("redirect:" + BASE_URL);
    }

    @GetMapping("/edit/{id}")
    public ModelAndView getEditForm(@PathVariable Long id) {
        AddressDto addressDto = addressService.findById(id);
        ModelAndView mav = new ModelAndView("address/edit-address");
        mav.addObject("address", addressDto);
        return mav;
    }

    @PostMapping("/edit/{id}")
    public RedirectView editAddress(@ModelAttribute("address") AddressDto addressDto, @PathVariable Long id) {
        addressService.editAddress(addressDto, id);
        return new RedirectView(BASE_URL);
    }
}