package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.dto.car.CarDto;
import pl.fudala.mateusz.rental.model.enums.CarType;
import pl.fudala.mateusz.rental.service.CarService;
import pl.fudala.mateusz.rental.service.DepartmentService;
import pl.fudala.mateusz.rental.utils.pagination.Paginator;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(CarController.BASE_URL)
public class CarController {

    public static final String BASE_URL = "/api/cars";
    private static final String REDIRECT = "redirect:";

    private final CarService carService;
    private final DepartmentService departmentService;

    @Autowired
    public CarController(CarService carService, DepartmentService departmentService) {
        this.carService = carService;
        this.departmentService = departmentService;
    }

    @GetMapping
    public ModelAndView allCars(@RequestParam(required = false) Integer page) {
        int pageNumber = (page == null ? 1 : page);
        int pageSize = 3;

        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, Sort.by("make"));
        Page<CarDto> cars = carService.findAll(pageable);

        ModelAndView mav = new ModelAndView("car/cars");
        mav.addObject("cars", cars);
        Paginator.addPageIndexes(mav,cars);

        return mav;
    }

    @GetMapping("add-car")
    public ModelAndView getCarForm() {
        ModelAndView mav = new ModelAndView("car/add-car");
        mav.addObject("car", new CarDto());
        mav.addObject("carTypes", CarType.values());
        mav.addObject("departments", departmentService.findAll());
        return mav;
    }

    @PostMapping("add-car")
    public ModelAndView addNewCar(@ModelAttribute("car") CarDto car) {
        carService.addCar(car);
        return new ModelAndView(REDIRECT + BASE_URL);
    }

    @GetMapping("search")
    public ModelAndView getCarsByMake(@RequestParam String make) {
        List<CarDto> carsByMake = carService.findByMake(make);
        ModelAndView mav = new ModelAndView("car/cars");
        mav.addObject("cars", carsByMake);
        return mav;
    }

    @GetMapping("edit/{id}")
    public ModelAndView editCar(@PathVariable Long id) {
        CarDto car = carService.findById(id);
        ModelAndView mav = new ModelAndView("car/edit-car");
        mav.addObject("car", car);
        mav.addObject("departments", departmentService.findAll());
        return mav;
    }

    @PostMapping("edit/{id}")
    public ModelAndView editCarForm(@ModelAttribute CarDto car, @PathVariable Long id) {
        carService.editCar(car, id);
        return new ModelAndView(REDIRECT + BASE_URL);
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteCar(@PathVariable Long id) {
        carService.deleteCarById(id);
        return new ModelAndView(REDIRECT + BASE_URL);
    }

    @GetMapping("/status")
    public ModelAndView carStatus(@RequestParam(required = false)
                           @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime searchDate) {

        ModelAndView mav = new ModelAndView("car/car-status");

        if (searchDate != null) {
            List<CarDto> allCars = carService.getListOfCarsOnProvidedDate(searchDate);
            mav.addObject("cars", allCars);
        }

        return mav;
    }

}