package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.dto.agreements.UnavailableDto;
import pl.fudala.mateusz.rental.service.CarService;
import pl.fudala.mateusz.rental.service.UnavailableService;
import pl.fudala.mateusz.rental.utils.pagination.Paginator;

@RestController
@RequestMapping(UnavailableController.BASE_URL)
public class UnavailableController {

    public static final String BASE_URL = "/api/unavailable";

    CarService carService;
    UnavailableService unavailableService;

    @Autowired
    public UnavailableController(CarService carService, UnavailableService unavailableService) {
        this.carService = carService;
        this.unavailableService = unavailableService;
    }

    @GetMapping({"/all", ""})
    public ModelAndView allUnavailableCars(@RequestParam(required = false) Integer page) {
        int pageNumber = (page == null ? 1 : page);
        int pageSize = 3;

        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, Sort.by("endDate"));

        Page<UnavailableDto> unavailables = unavailableService.findAll(pageable);

        ModelAndView mav = new ModelAndView("unavailable/all");
        mav.addObject("unavailables", unavailables);
        Paginator.addPageIndexes(mav,unavailables);

        return mav;
    }

    @GetMapping("/add")
    public ModelAndView showAddForm() {
        ModelAndView mav = new ModelAndView("/unavailable/add-unavailable");
        mav.addObject("cars", carService.findAll());
        mav.addObject("unavailable", new UnavailableDto());
        return mav;
    }

    @PostMapping("/add")
    public ModelAndView addUnavailable(@ModelAttribute("unavailable") UnavailableDto unavailableDto) {
        unavailableService.addUnavailable(unavailableDto);
        return new ModelAndView("redirect:" + BASE_URL);
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteUnavailable(@PathVariable("id") Long id) {
        unavailableService.deleteById(id);
        return new ModelAndView("redirect:" + BASE_URL);
    }
}
