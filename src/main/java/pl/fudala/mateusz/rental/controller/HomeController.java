package pl.fudala.mateusz.rental.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.dto.users.UserRegistrationDto;

@RestController
@RequestMapping("/")
public class HomeController {

    @GetMapping({"/login", "/"})
    public ModelAndView redirectToLogin() {
        ModelAndView mav = new ModelAndView("logging/login");
        mav.addObject("user", new UserRegistrationDto());
        return mav;
    }

    @GetMapping("/accessDenied")
    public ModelAndView accessDenied() {
        return new ModelAndView("error/403");
    }
}
