package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.revenues.ReservationRevenueDto;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;

import java.util.List;

public interface ReservationRevenueService {
    List<ReservationRevenueDto> findAll();

    List<ReservationRevenueDto> findActive();

    List<ReservationRevenueDto> findCancelled();

    ReservationRevenueDto findById(Long id);

    String getSumOfReservationRevenues();

    String getSumOfActiveReservationRevenues();

    String getSumOfCancelledReservationRevenues();

    Page<ReservationRevenueDto> findPaginated(Pageable pageable);

    void addReservationRevenue(ReservationEntity reservationEntity);

    void calculateRevenuesWhenCancelRes(ReservationEntity reservationEntity);

}
