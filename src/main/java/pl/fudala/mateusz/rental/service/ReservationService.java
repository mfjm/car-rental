package pl.fudala.mateusz.rental.service;

import pl.fudala.mateusz.rental.dto.agreements.ReservationDto;

import java.math.BigDecimal;
import java.util.List;

public interface ReservationService {
    List<ReservationDto> findAll();

    List<ReservationDto> findAllNotRealised();

    BigDecimal calculatePrice(ReservationDto reservationDto);

    void addReservation(ReservationDto reservationDto);

    void deleteReservationById(Long id);

    ReservationDto findById(Long id);

    void editReservation(ReservationDto reservationDto);
}
