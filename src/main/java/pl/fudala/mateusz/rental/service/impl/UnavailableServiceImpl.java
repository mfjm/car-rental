package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.agreements.UnavailableDto;
import pl.fudala.mateusz.rental.model.agreements.UnavailableEntity;
import pl.fudala.mateusz.rental.repository.UnavailableRepository;
import pl.fudala.mateusz.rental.service.UnavailableService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UnavailableServiceImpl implements UnavailableService {

    private final UnavailableRepository unavailableRepository;

    @Autowired
    public UnavailableServiceImpl(UnavailableRepository unavailableRepository) {
        this.unavailableRepository = unavailableRepository;
    }

    @Override
    public void addUnavailable(UnavailableDto unavailableDto) {
        unavailableRepository.save(Mapper.map(unavailableDto));
    }

    @Override
    public void deleteById(Long id) {
        unavailableRepository.deleteById(id);
    }

    @Override
    public UnavailableDto findById(Long id) {
        UnavailableEntity unavailableEntity =
                unavailableRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        return Mapper.map(unavailableEntity);
    }

    @Override
    public List<UnavailableDto> findAll() {
        return unavailableRepository.findAll().stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public Page<UnavailableDto> findAll(Pageable pageable) {
        return unavailableRepository.findAll(pageable).map(Mapper::map);
    }
}