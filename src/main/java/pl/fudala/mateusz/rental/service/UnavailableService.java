package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.agreements.UnavailableDto;

import java.util.List;

public interface UnavailableService {

    void addUnavailable(UnavailableDto unavailableDto);
    void deleteById(Long id);
    UnavailableDto findById(Long id);
    List<UnavailableDto> findAll();
    Page<UnavailableDto> findAll(Pageable pageable);
}
