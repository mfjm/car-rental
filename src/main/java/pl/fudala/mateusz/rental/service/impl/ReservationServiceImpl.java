package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.agreements.ReservationDto;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.repository.ReservationRepository;
import pl.fudala.mateusz.rental.service.ReservationRevenueService;
import pl.fudala.mateusz.rental.service.ReservationService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;
    private final ReservationRevenueService reservationRevenueService;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository,
                                  ReservationRevenueService reservationRevenueService) {
        this.reservationRepository = reservationRepository;
        this.reservationRevenueService = reservationRevenueService;
    }

    @Override
    public List<ReservationDto> findAll() {
        return reservationRepository.findAll().stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationDto> findAllNotRealised() {
        return reservationRepository.findAllNotRealised().stream().map(Mapper::map).collect(Collectors.toList());
    }

    @Override
    public BigDecimal calculatePrice(ReservationDto reservationDto) {
        LocalDateTime startDate = reservationDto.getRentalStartDate();
        LocalDateTime endDate = reservationDto.getRentalEndDate();
        BigDecimal perDay = reservationDto.getCar().getPricePerDay();
        long days = Duration.between(startDate, endDate).toDays();
        return perDay.multiply(BigDecimal.valueOf(days));
    }

    @Override
    public void addReservation(ReservationDto reservationDto) {
        ReservationEntity reservationEntity = Mapper.map(reservationDto);
        ReservationEntity savedRes = reservationRepository.save(reservationEntity);
        reservationRevenueService.addReservationRevenue(savedRes);
    }

    @Override
    public void deleteReservationById(Long id) {
        ReservationEntity reservationById = reservationRepository.findById(id)
                .orElseThrow(IllegalArgumentException::new);
        reservationRevenueService.calculateRevenuesWhenCancelRes(reservationById);
        reservationRepository.deleteById(id);

    }

    @Override
    public ReservationDto findById(Long id) {
        return Mapper.map(reservationRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }


    @Override
    public void editReservation(ReservationDto reservationDto) {
        ReservationEntity reservationEntity = reservationRepository.findById(reservationDto.getId())
                .orElseThrow(IllegalArgumentException::new);
        reservationEntity.setReservationDate(reservationDto.getReservationDate());
        reservationEntity.setCar(reservationDto.getCar());
        reservationEntity.setRentalStartDate(reservationDto.getRentalStartDate());
        reservationEntity.setRentalEndDate(reservationDto.getRentalEndDate());
        reservationEntity.setDepartureDepartment(reservationDto.getCar().getDepartment());
        reservationEntity.setArrivalDepartment(reservationDto.getArrivalDepartment());
        reservationEntity.setPrice(calculatePrice(reservationDto));
        reservationEntity.setReservationDate(LocalDateTime.now());

    }

}
