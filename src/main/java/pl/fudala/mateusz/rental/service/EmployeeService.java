package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.person.EmployeeDto;

@Service
public interface EmployeeService {
    void add(EmployeeDto employeeDto);
    Page<EmployeeDto> findAll(Pageable pageable);
    EmployeeDto findById(Long id);
    void deleteById(Long id);
}
