package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.person.AddressDto;

import java.util.List;

public interface AddressService {
    List<AddressDto> findAll();

    void addNewAddress(AddressDto addressDto);

    void deleteAddressById(Long id);

    void editAddress(AddressDto addressDto, Long id);

    AddressDto findById(Long id);
    Page<AddressDto> findAll(Pageable pageable);
}
