package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.revenues.RentalRevenueDto;
import pl.fudala.mateusz.rental.model.agreements.RentalEntity;
import pl.fudala.mateusz.rental.model.revenues.RentalRevenueEntity;
import pl.fudala.mateusz.rental.repository.RentalRevenueRepository;
import pl.fudala.mateusz.rental.service.RentalRevenueService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RentalRevenueServiceImpl implements RentalRevenueService {

    private final RentalRevenueRepository rentalRevenueRepository;

    @Autowired
    public RentalRevenueServiceImpl(RentalRevenueRepository rentalRevenueRepository) {
        this.rentalRevenueRepository = rentalRevenueRepository;
    }

    @Override
    public List<RentalRevenueDto> findAll() {
        return rentalRevenueRepository.findAll()
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public RentalRevenueDto findById(Long id) {
        return Mapper.map(rentalRevenueRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

    @Override
    public String getSumOfRentalRevenues() {
        Optional<BigDecimal> revenues = rentalRevenueRepository.getSumOfRentalRevenues();
        if (revenues.isPresent()) {
            BigDecimal bigDecimal = revenues.orElseThrow(IllegalArgumentException::new);
            return String.valueOf(bigDecimal.setScale(BigDecimal.ROUND_CEILING, 2));
        } else {
            return String.valueOf(0);
        }
    }

    @Override
    public String getSumOfAllRevenues() {
        Optional<BigDecimal> revenues = rentalRevenueRepository.getSumOfAllRevenues();
        if (revenues.isPresent()) {
            BigDecimal bigDecimal = revenues.orElseThrow(IllegalArgumentException::new);
            return String.valueOf(bigDecimal.setScale(BigDecimal.ROUND_CEILING, 2));
        } else {
            return String.valueOf(0);
        }
    }

    @Override
    public Page<RentalRevenueDto> findPaginated(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<RentalRevenueDto> list;
        List<RentalRevenueDto> allRentalRevenues = findAll();
        if (allRentalRevenues.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allRentalRevenues.size());
            list = allRentalRevenues.subList(startItem, toIndex);
        }
        return new PageImpl<>(list,
                PageRequest.of(currentPage, pageSize),
                allRentalRevenues.size());
    }

    @Override
    public void createRentalRevenue(RentalEntity rentalEntity) {
        RentalRevenueEntity rentalRevenueEntity = new RentalRevenueEntity();
        rentalRevenueEntity.setRentalValue(rentalEntity.getReservation().getPrice());
        rentalRevenueRepository.save(rentalRevenueEntity);
    }
}
