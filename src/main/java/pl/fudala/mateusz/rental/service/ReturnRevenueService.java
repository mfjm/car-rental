package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.revenues.ReturnRevenueDto;

import java.math.BigDecimal;

public interface ReturnRevenueService {
    Page<ReturnRevenueDto> findAll(Pageable pageable);

    BigDecimal sumOfSurcharges();
}
