package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.person.AddressDto;
import pl.fudala.mateusz.rental.model.company.AddressEntity;
import pl.fudala.mateusz.rental.repository.AddressRepository;
import pl.fudala.mateusz.rental.service.AddressService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public List<AddressDto> findAll() {
        return addressRepository.findAll()
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void addNewAddress(AddressDto addressDto) {
        addressRepository.save(Mapper.map(addressDto));
    }

    @Override
    public void deleteAddressById(Long id) {
        addressRepository.deleteById(id);
    }

    @Override
    public void editAddress(AddressDto addressDto, Long id) {
        AddressEntity addressEntity = addressRepository
                .findById(id)
                .orElseThrow(IllegalArgumentException::new);
        addressEntity.setCity(addressDto.getCity());
        addressEntity.setHouseNumber(addressDto.getHouseNumber());
        addressEntity.setPostalCode(addressDto.getPostalCode());
        addressEntity.setStreet(addressDto.getStreet());
        addressRepository.save(addressEntity);
    }

    @Override
    public AddressDto findById(Long id) {
        return Mapper.map(addressRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

    @Override
    public Page<AddressDto> findAll(Pageable pageable) {
        return addressRepository.findAll(pageable).map(Mapper::map);
    }

}
