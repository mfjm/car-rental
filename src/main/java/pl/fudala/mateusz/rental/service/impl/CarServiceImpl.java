package pl.fudala.mateusz.rental.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.car.CarDto;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.repository.CarRepository;
import pl.fudala.mateusz.rental.repository.DepartmentRepository;
import pl.fudala.mateusz.rental.service.CarService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static pl.fudala.mateusz.rental.model.enums.CarRentalStatus.*;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final DepartmentRepository departmentRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository, DepartmentRepository departmentRepository) {
        this.carRepository = carRepository;
        this.departmentRepository = departmentRepository;
    }

    @Override
    public List<CarDto> findByMake(String make) {
        return carRepository.findByMakeIgnoreCase(make)
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public CarDto findById(Long id) {
        CarEntity carById = carRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        return Mapper.map(carById);
    }

    @Override
    public void addCar(CarDto carDto) {
        CarEntity newCarEntity = Mapper.map(carDto);
        carRepository.save(newCarEntity);
        DepartmentEntity department = departmentRepository
                .findByDepartmentName(newCarEntity.getDepartment()
                        .getDepartmentName());
        department.getCars().add(newCarEntity);
        departmentRepository.save(department);
    }

    @Override
    public void editCar(CarDto carDto, Long id) {
        CarEntity carEntity = carRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        DepartmentEntity departmentRemove = departmentRepository
                .findByDepartmentName(carEntity.getDepartment()
                        .getDepartmentName());
        List<CarEntity> carsToRemove = departmentRemove.getCars();
        CarEntity any = carsToRemove.stream().filter(c -> c.getId().equals(carEntity.getId())).findAny().orElseThrow(IllegalArgumentException::new);
        carsToRemove.remove(any);
        departmentRemove.setCars(carsToRemove);
        departmentRepository.save(departmentRemove);
        carEntity.setRentalStatus(carDto.getRentalStatus());
        carEntity.setPricePerDay(carDto.getPricePerDay());
        carEntity.setCarColor(carDto.getCarColor());
        carEntity.setDepartment(carDto.getDepartment());
        carEntity.setReservation(carDto.getReservation());
        carEntity.setCarType(carDto.getCarType());
        carEntity.setMake(carDto.getMake());
        carEntity.setMileage(carDto.getMileage());
        carEntity.setModel(carDto.getModel());
        carRepository.save(carEntity);
        DepartmentEntity departmentAdd = departmentRepository
                .findByDepartmentName(carEntity.getDepartment()
                        .getDepartmentName());
        List<CarEntity> cars = departmentAdd.getCars();
        cars.add(carEntity);
        departmentAdd.setCars(cars);
        departmentRepository.save(departmentAdd);
    }

    @Override
    public void deleteCarById(Long id) {
        CarEntity carEntity = carRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        DepartmentEntity department = departmentRepository.findByDepartmentName(carEntity.getDepartment().getDepartmentName());
        department.getCars().remove(carEntity);
        departmentRepository.save(department);
        carRepository.delete(carEntity);
    }

    @Override
    public List<CarDto> findAll() {
        return carRepository.findAll()
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public Page<CarDto> findAll(Pageable pageable) {
        return carRepository.findAll(pageable).map(Mapper::map);
    }

    @Override
    public List<CarDto> findAvailableCarsByDepartment(Long departmentId) {
        return carRepository.findAvailableCarsByDepartmentId(departmentId).stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<CarDto> findAllReservedCarsInProvidedDate(LocalDateTime dateTime) {
        List<CarEntity> reservedCars = carRepository.findAllReservedCarsInProvidedDate(dateTime);
        reservedCars.forEach(car -> car.setRentalStatus(RESERVED));

        return reservedCars.stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<CarDto> findAllRentedCarsInProvidedDate(LocalDateTime dateTime) {
        List<CarEntity> rentedCars = carRepository.findAllRentedCarsInProvidedDate(dateTime);
        rentedCars.forEach(car -> car.setRentalStatus(RENTED));

        return rentedCars.stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<CarDto> findAllUnavailableCarsInProvidedDate(LocalDateTime dateTime) {
        List<CarEntity> unavailableCars = carRepository.findAllUnavailableCarsInProvidedDate(dateTime);
        unavailableCars.forEach(car -> car.setRentalStatus(UNAVAILABLE));

        return unavailableCars.stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<CarDto> getListOfCarsOnProvidedDate(LocalDateTime dateTime) {

        List<CarDto> reservedCars = findAllReservedCarsInProvidedDate(dateTime);
        List<CarDto> rentedCars = findAllRentedCarsInProvidedDate(dateTime);
        List<CarDto> unavailableCars = findAllUnavailableCarsInProvidedDate(dateTime);

        List<Long> reservedCarIDs = getListOfIDsFromCarDtoList(reservedCars);
        List<Long> rentedCarIDs = getListOfIDsFromCarDtoList(rentedCars);
        List<Long> unavailableCarId = getListOfIDsFromCarDtoList(unavailableCars);

        List<Long> carsWithUpdatedStatus = new ArrayList<>();
        carsWithUpdatedStatus.addAll(reservedCarIDs);
        carsWithUpdatedStatus.addAll(rentedCarIDs);
        carsWithUpdatedStatus.addAll(unavailableCarId);

        List<CarDto> allCars = findAll();
        allCars.forEach(c -> c.setRentalStatus(AVAILABLE));

        allCars.removeIf(c -> carsWithUpdatedStatus.contains(c.getId()));

        allCars.addAll(rentedCars);
        allCars.addAll(reservedCars);
        allCars.addAll(unavailableCars);

        return allCars;
    }


    @Override
    public List<Long> getListOfIDsFromCarDtoList(List<CarDto> carDtoList) {
        return carDtoList.stream()
                .map(CarDto::getId)
                .collect(Collectors.toList());
    }
}
